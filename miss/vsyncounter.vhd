----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/20/2021 06:54:20 PM
-- Design Name: 
-- Module Name: vsyncounter - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity vsyncounter is
    Port ( IPSAMPLEALLOW : in STD_LOGIC;
           counter : out STD_LOGIC_VECTOR(31 downto 0) := (others => '0');
           VSYNC : in STD_LOGIC;
           rstct : in STD_LOGIC
           );
end vsyncounter;

architecture Behavioral of vsyncounter is
signal cout :STD_LOGIC_VECTOR(31 downto 0) := (others => '0');
begin

counter<=cout;

process(vsync,rstct)is 
begin
if(rstct='1')then
cout<=(others=>'0');
else
if(IPSAMPLEALLOW='1')then
if(rising_edge(vsync))then
cout<=std_logic_vector(unsigned(cout)+1);
end if; 
end if;
end if;
end process;


end Behavioral;
